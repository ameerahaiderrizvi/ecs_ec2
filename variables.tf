
variable "aws_access_key" {
    default = "AKIAWGCDKTVXQHIGLZMO"
}
variable "aws_secret_key" {
    default = "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"
}
variable "aws_region" {
  default = "us-east-1"
}

variable "name_prefix" {
  description = "The prefix to use for all resource names"
  type = string
  default = "A"
}

variable "availability_zones" {
  description = "List of availability zones for subnet association"
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b"]
}

variable "vpc_cidr" {
  description = "The CIDR block for the VPC"
  type = string
  default = "192.168.16.0/20"
}

variable "public_subnets" {
  description = "The CIDR blocks for the public subnets"
  type = list(string)
  default = ["192.168.16.0/24","192.168.17.0/24","192.168.18.0/24"]
}

variable "ami_id" {
  description = "AMI of Linux 2"
  type        = string
  default     = "ami-0e13330257b20a8e4"
}

variable "key_name" {
  description = "Key Pair Name"
  type        = string
  default     = "my-key-pair"
}
