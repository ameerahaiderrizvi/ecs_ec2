terraform {
  backend "s3" {
    bucket  = "my-terraform-state-bucket-cicd"
    key     = "terraform/state/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

module "vpc" {
  source = "./modules/vpc"
  vpc_cidr = var.vpc_cidr
  name_prefix = var.name_prefix
  public_subnets = var.public_subnets
  availability_zones = var.availability_zones
}

module "sg" {
  source = "./modules/sg"
  vpc_id = module.vpc.vpc_id
  name_prefix = var.name_prefix
}

//Auto Scaling Group
data "template_file" "appData" {
  template = file("./app_userdata.sh")
}

module "autoscaling_group" {
  source               = "./modules/autoscaling_group"
  name_prefix          = var.name_prefix
  ami_id               = var.ami_id
  instance_type        = "t2.large"
  key_name             = var.key_name
  min_size             = 1

  max_size             = 5
  desired_capacity     = 1
  security_group_id    = module.sg.app_SG_id
  public_subnet_ids   = module.vpc.public_subnets_ids
  user_data            = data.template_file.appData.rendered
}

module "ecs" {
  source = "./modules/ecs"
  security_group_id    = module.sg.app_SG_id
  public_subnets_ids = module.vpc.public_subnets_ids
  ASG_ARN = module.autoscaling_group.ASG_ARN
}




