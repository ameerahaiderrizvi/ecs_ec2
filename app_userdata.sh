#!/bin/bash

# Set ECS_CLUSTER environment variable
echo 'ECS_CLUSTER=PythonApp-Cluster-EC2' | sudo tee -a /etc/ecs/ecs.config


#!/bin/bash

# Update the package information
sudo yum update -y

# Install Docker
sudo amazon-linux-extras install -y docker
sudo systemctl start docker
sudo systemctl enable docker

# Create ECS configuration directory
sudo mkdir -p /etc/ecs

# Set up ECS config with AWS credentials
cat <<EOF | sudo tee /etc/ecs/ecs.config > /dev/null
ECS_CLUSTER=YOUR_ECS_CLUSTER_NAME
ECS_ENGINE_AUTH_TYPE=dockercfg
ECS_ENGINE_AUTH_DATA={"https://index.docker.io/v1/":{"auth":"$(echo -n 'AWS_ACCESS_KEY_ID:AWS_SECRET_ACCESS_KEY' | base64)","email":"YOUR_EMAIL"}}
EOF

# Replace placeholders with your values
sudo sed -i 's/YOUR_ECS_CLUSTER_NAME/your_actual_ecs_cluster_name/' /etc/ecs/ecs.config
sudo sed -i 's/AWS_ACCESS_KEY_ID/your_actual_access_key/' /etc/ecs/ecs.config
sudo sed -i 's/AWS_SECRET_ACCESS_KEY/your_actual_secret_key/' /etc/ecs/ecs.config
sudo sed -i 's/YOUR_EMAIL/your_actual_email/' /etc/ecs/ecs.config

# Run ECS agent
sudo docker run --name ecs-agent \
-d \
--restart=unless-stopped \
--net=host \
-v /var/run:/var/run \
-v /var/log/ecs:/log \
-v /var/lib/ecs/data:/data \
-v /etc/ecs:/etc/ecs \
amazon/amazon-ecs-agent:latest
