output "app_SG_id" {
  description = "The ID of the Jump Server SG"
  value       = aws_security_group.my_security_group.id
}