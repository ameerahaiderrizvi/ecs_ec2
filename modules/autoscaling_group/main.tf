resource "aws_launch_template" "launch_template" {
  name_prefix            = "${var.name_prefix}-launch-template"
  image_id               = var.ami_id
  instance_type          = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [var.security_group_id]
  user_data              = base64encode(var.user_data)

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg" {
  name                       = "${var.name_prefix}-asg-instance"
  desired_capacity           = var.desired_capacity
  max_size                   = var.max_size
  min_size                   = var.min_size
  vpc_zone_identifier        = var.public_subnet_ids
  health_check_type          = "EC2"  # Change health check type to EC2
  health_check_grace_period  = 300    # Set health check grace period to 300 seconds (5 minutes)
  protect_from_scale_in = true

  launch_template {

    id      = aws_launch_template.launch_template.id
    version = "$Latest"
  }

  tag {
    key                 = "AmazonECSManaged"
    value               = true
    propagate_at_launch = true
  }

  tag {
    key                 = "Name"
    value               = "ECS Instance"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
