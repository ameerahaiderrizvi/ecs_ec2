data "aws_instances" "example" {
  instance_tags = {
    Name = "${var.name_prefix}-asg-instance"
  }
}

output "public_ips" {
  value = data.aws_instances.example.public_ips
}

output "ips" {
  value = [for ip in data.aws_instances.example.public_ips : "${ip}"]
}

output "ASG_ARN" {
  value = aws_autoscaling_group.asg.arn
}

